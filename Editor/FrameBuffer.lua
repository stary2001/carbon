framebuffer = {}
Table = {}

function framebuffer:new(x, y)
	-- Create tables
	textBuffer = {}
	textColorBuffer = {}
	backgroundColorBuffer = {}
	-- Fill with the default values.
	for xi = 1, x do
		yTextBuffer = {}
		yTextColorBuffer = {}
		yBackgroundColorBuffer = {}
		for yi = 1, y do
			table.insert(yTextBuffer, " ")
			table.insert(yTextColorBuffer, colors.white)
			table.insert(yBackgroundColorBuffer, colors.black)
		end
		table.insert(textBuffer, yTextBuffer)
		table.insert(textColorBuffer, yTextColorBuffer)
		table.insert(backgroundColorBuffer, yBackgroundColorBuffer)
	end
	-- Garbage  collection
	yTextBuffer = nil
	yTextColorBuffer = nil
	yBackgroundColorBuffer = nil
	-- Return meta table
	return setmetatable({textBuffer = textBuffer, textColorBuffer = textColorBuffer, backgroundColorBuffer = backgroundColorBuffer, sx = x, sy = y, x = 1, y = 1, currentTextColor = colors.white, currentBackgroundColor = colors.black}, Table)
end

function framebuffer:setCursorPos(x, y)
	self.x, self.y = x, y
end

function framebuffer:setColor(textcolor, bgcolor)
	self.currentTextColor = textcolor
	self.currentBackgroundColor = bgcolor
end

function framebuffer:setTextColor(textcolor, bgcolor)
	self.currentTextColor = textcolor
end

function framebuffer:setBackgroundColor(bgcolor)
	self.currentBackgroundColor = bgcolor
end

function framebuffer:print(text, x, y)
	if x and y then self:setCursorPos(x, y) end
	currentX = self.x
	currentY = self.y
	for i = 1, text:len() do
		self.textBuffer[x][y] = text:sub(i,i)
		self.textColorBuffer[x][y] = self.currentTextColor
		self.backgroundColorBuffer[x][y] = self.currentBackgroundColor
		currentX = currentX + 1
		if currentX > self.sx then currentX = 1; currentY = currentY + 1 end
		if currentY > self.sy then currentY = 1; end
	end
	self:setCursorPos(currentX+1, currentY)
end

function framebuffer:clear()
	textBuffer = {}
	textColorBuffer = {}
	backgroundColorBuffer = {}
	for xi = 1, self.sx do
		yTextBuffer = {}
		yTextColorBuffer = {}
		yBackgroundColorBuffer = {}
		for yi = 1, self.sy do
			table.insert(yTextBuffer, " ")
			table.insert(yTextColorBuffer, colors.white)
			table.insert(yBackgroundColorBuffer, colors.black)
		end
		table.insert(textBuffer, yTextBuffer)
		table.insert(textColorBuffer, yTextColorBuffer)
		table.insert(backgroundColorBuffer, yBackgroundColorBuffer)
	end
	yTextBuffer = nil
	yTextColorBuffer = nil
	yBackgroundColorBuffer = nil
	self.textBuffer = textBuffer
	self.textColorBuffer = textColorBuffer
	self.backgroundColorBuffer = backgroundColorBuffer
end

function framebuffer:draw(x, y, sx, sy, px, py)
	px = px or 1
	py = py or 1
	for ix = -1,sx do
		for iy = -1,sy do
			term.setTextColor(self.textColorBuffer[x+ix+px][y+iy+py])
			term.setBackgroundColor(self.backgroundColorBuffer[x+ix+px][y+iy+py])
			term.setCursorPos(x+ix+py, y+iy+py)
			term.write(self.textBuffer[x+ix+px][y+iy+py])
		end
	end
end

function newFramebuffer(x, y)
	return framebuffer:new(x, y)
end