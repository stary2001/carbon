-- Variables
x, y = 1,1
local path = shell.getRunningProgram()
path = string.gsub(path,fs.getName(path),"")
path = string.sub(path,1,#path-1)
args = {...}
patharg = args[1] or path .."/files/default.page"
tx, ty = term.getSize()

-- Internal APIs
setTextColor = (term.isColor() and term.setTextColor) or function() end
setBackgroundColor = (term.isColor() and term.setBackgroundColor) or function() end

-- External APIs
os.loadAPI(path .."/framebuffer")
print(framebuffer)
localFramebuffer = framebuffer.newFramebuffer(500, 500)
file = fs.open(patharg, "r")

if file then
	xml = false
	repeat
		line = file.readLine()
		if line then --ToDo Add colour parsing.
			if line:find("<xml>") then xml = true end
			if xml then
				if line.find("</xml>") then xml = false end
				cfind1, cfind2, cCapture1, cCapture2 =  line:find("<.+bgcolor=\"(%a)\" color=\"(%a)\">")
				pfind1, pfind2, pCapture1, pCapture2 = line:find("<.+pos=\"(%n),(%n)\">")
				if cfind1 then
					if cCapture1 and cCapture2 then
						localFramebuffer:setColor(colors[cCapture1], colors[cCapture2])
					end
				elseif pfind1 and pfind2 then
					pCapture1 = tonumber(pCapture1) or 0
					pCapture2 = tonumber(pCapture2) or 0
					localFramebuffer:setCursorPos(pCapture1, pCapture2)
				else
					tfind1, tfind2, capture1 = line:find("\t*(.+)")
					if tfind1 and capture1 then
						localFramebuffer:print(capture1)
					end
				end
			end
		end
	until not line
end

localFramebuffer:draw(1, 1, term.getSize())