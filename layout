layout={}

local inherit = {"bgcolor","color","align"}

function contains(t,c)
	for k,v in pairs(t) do
		if c == v then return true end
	end
	return false
end

function tablelength(t)
  local count = 0
  for _ in pairs(t) do count = count + 1 end
  return count
end


local maxX,maxY
maxX=0
maxY=0

function layout.getBounds()
	return {x=maxX, y=maxY}
end

function process(node)
	if node.pos and node.size and node.pos.x + node.size.x > maxX then
		maxX=node.pos.x + node.size.x
	end
	
	if node.pos and node.size and node.pos.y + node.size.y > maxY then
		maxY=node.pos.y + node.size.y
	end
	
	if node.data then
		for k,v in pairs(node.data) do
			if contains(inherit, k) then
				if type(node.content)=="table" then
					for k2,v2 in pairs(node.content) do
						if not v2.data[k] then
							v2.data[k] = v
						end
						process(v2)
					end
				end
			end
		end
	end
	
	if type(node.content)=="table" then
		for k,v in pairs(node.content) do
			if v.type=="text" and node.pos then
				v.pos = node.pos
				break
			end
		end
	end
end

local xOff,yOff
xOff=0
yOff=0

function layout.setOffset(x,y)
	xOff=x
	yOff=y
end

function transform(pos)
	return {x=pos.x + xOff,y=pos.y + yOff + 1}
end

framebuffer={}
local framebufferX=0
local framebufferY=0
local curTextColor,curBackgroundColor

function framebufferSetCursorPos(x,y)
	framebufferX=x
	framebufferY=y
end

function framebufferSetTextColor(color)
	curTextColor = color
end

function framebufferSetBackgroundColor(color)
	curBackgroundColor = color
end

function framebufferPrint(str)
	for i=1,#str do
		if not framebuffer[framebufferX] then
			framebuffer[framebufferX]={}
		end
	
		framebuffer[framebufferX][framebufferY]={char=str:sub(i,i),fgCol=curTextColor,bgCol=curBackgroundColor}
		framebufferX=framebufferX+1
	end
end

function getColor(colorStr)
	return colors[colorStr]
end

function layout.render(node)
	if node.dirty then
		if node.type == "text" then
			local pos = transform(node.pos)
			if node.data.align then
				local xs,ys = term.getSize()
				if node.data.align=="left" then
					-- do nothing
				elseif node.data.align=="right" then
					pos={x=(xs + pos.x) - node.size.x,y=pos.y}
				elseif node.data.align=="center" then
					pos={x=(xs / 2) - math.ceil(node.size.x / 2) ,y=pos.y}
				end
			end
			
			framebufferSetCursorPos(pos.x,pos.y)
			if node.data.color then framebufferSetTextColor(getColor(node.data.color)) end
			if node.data.bgcolor then framebufferSetBackgroundColor(getColor(node.data.bgcolor)) end
			framebufferPrint(node.content)
				
			framebufferSetTextColor(colors.white)
			framebufferSetBackgroundColor(colors.black)

		elseif type(node.content) == "table" then
			for k,v in ipairs(node.content) do
				layout.render(v)
			end
		end
		node.dirty=false
	end
end


local next_pos = {x=1,y=1}
function layout.nextpos(node)
	local p = {x=next_pos.x,y=next_pos.y}
	next_pos.y = next_pos.y + 1
	return p
end

function needsPos(type)
	if type == "text" then
		return true
	end
	return false
end

function layout.new(type, pos, size, data, content)
	local node = {["type"]    = type,
				  ["pos"]     = pos or (needsPos(type) and layout.nextpos()),
				  ["size"]    = size,
				  ["data"]    = data,
				  ["content"] = content,
				  ["dirty"]   = true} -- written to framebuffer?
	
	process(node)
	
	return node
end